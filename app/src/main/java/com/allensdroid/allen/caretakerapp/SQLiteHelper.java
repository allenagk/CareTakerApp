package com.allensdroid.allen.caretakerapp;

/**
 * Created by anjanee on 22-Jun-16.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.HashMap;

/**
 * Created by Nikunj on 27-08-2015.
 */
public class SQLiteHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "CaretakerDB.db";
    public static final String TABLE_NAME1 = "health";          //Health Details Table
    public static final String TABLE_NAME2 = "health_count";    //Thread to get the current health count
    public static final String TABLE_NAME3 = "user";            //user login details held
    //public static final String COLUMN_ID = "id";
    public static final String PATIENT_USERID = "user_name";
    public static final String TIME = "time";
    public static final String HEARTRATE = "heartrate";
    public static final String PRESSURE = "pressure";
    public static final String LOCATION = "location";
    public static final String CONDITION = "condition";
    public static final String OCCASSION = "occasion";
    //private HashMap hp;

    public SQLiteDatabase database;

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+ TABLE_NAME1+" (_id INTEGER PRIMARY KEY AUTOINCREMENT, " + TIME + " TEXT, " + HEARTRATE + " DOUBLE, " + PRESSURE + " DOUBLE, " + LOCATION + " TEXT, " + CONDITION + " TEXT, " + OCCASSION + " TEXT );");
        db.execSQL("CREATE TABLE "+ TABLE_NAME2+" (counts INT);");
        db.execSQL("CREATE TABLE "+ TABLE_NAME3+" (u_id INTEGER PRIMARY KEY," + PATIENT_USERID + " VARCHAR );");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME1);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME2);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME3);
        onCreate(db);
    }

    public void insertRecord(JSONDecoder jsonDecoder) {
        database = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(TIME, jsonDecoder.getDate());
        values.put(HEARTRATE, jsonDecoder.getHeartrate());
        values.put(PRESSURE, jsonDecoder.getPressure());
        values.put(LOCATION, jsonDecoder.getLocation());
        values.put(CONDITION, jsonDecoder.getCondition());
        values.put(OCCASSION, jsonDecoder.getOccasion());

        database.insert(TABLE_NAME1, TIME, values);
        database.close();
    }

    public void insertPatient_UserId(String patient_username){
        database = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(PATIENT_USERID, patient_username);
        database.insert(TABLE_NAME3,PATIENT_USERID,values);
        database.close();
    }

    public String retrieveLatestPatient_UserId(){
        database = this.getReadableDatabase();
        Cursor cursor =  database.rawQuery( "SELECT * FROM "+TABLE_NAME3, null );
        cursor.moveToLast();
        String patient_userid=null;
        patient_userid=cursor.getString(cursor.getColumnIndex(PATIENT_USERID));
        return patient_userid;
    }


    public String getFirstRow(int columnNo){        //get latest row
        database = this.getReadableDatabase();
        Cursor cursor = database.query(TABLE_NAME1, null, null, null, null, null, null);
        cursor.moveToLast();
        String a=null;
        a=cursor.getString(columnNo);
        return a;
    }

    public String getAllRows() {
        database = this.getReadableDatabase();
        Cursor cursor = database.query(TABLE_NAME1, null, null, null, null, null, null);
        String a=null;
        if (cursor.getCount() > 0) {
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToNext();


                a= cursor.getString(3);



            }
        }
        cursor.close();
        database.close();

        return a;
    }

    public void resetTable(){
        database = this.getWritableDatabase();
        database.execSQL("DELETE FROM "+TABLE_NAME1);
        onCreate(this.database);
        database.close();
    }

    public String userLogname(){
        database = this.getReadableDatabase();
        Cursor cursor = database.query(TABLE_NAME3,null,null,null,null,null,null);
        cursor.moveToFirst();
        String logname = cursor.getString(1);
        return logname;
    }

    public void deleteAll(String tablename) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(tablename,null,null);
        db.execSQL("DELETE FROM "+ tablename);
        db.close();
    }

    public long countRows(String table_name) {
        database = this.getReadableDatabase();
        long numRows = DatabaseUtils.queryNumEntries(database, table_name);
        return numRows;
    }

    public void insertCount(int count){
        database = this.getWritableDatabase();
        database.delete(TABLE_NAME2,null,null);
        database.execSQL("DELETE FROM "+ TABLE_NAME2);
        ContentValues values = new ContentValues();
        values.put("counts", count);
        database.insert(TABLE_NAME2,"counts",values);
        database.close();
    }

    public int valueOfHealthCount(){
        database = this.getReadableDatabase();
        Cursor cursor = database.query(TABLE_NAME2,null,null,null,null,null,null);
        cursor.moveToFirst();
        int count = cursor.getInt(0);
        return count;
    }

}

