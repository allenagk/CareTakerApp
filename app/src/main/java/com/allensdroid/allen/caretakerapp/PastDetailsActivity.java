package com.allensdroid.allen.caretakerapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PastDetailsActivity extends Activity{
    SQLiteHelper sqLiteHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_details);
        sqLiteHelper = new SQLiteHelper(this);

        String food[]={"apple","mango","banaa","god"};
        ListAdapter listAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,food);
        ListView listView=(ListView)findViewById(R.id.list_item1);
        listView.setAdapter(listAdapter);


    }

    public void onClickHome(View view){
        //finish();
        //long row = sqLiteHelper.countRows("user");
       // Toast.makeText(this, String.valueOf(row),Toast.LENGTH_SHORT).show();
        /*
        String jsonString="{\"Condition\": \"Abnormal\",\"Occasion\": \"Critical\",\"Heartrate\": 120.5,\"Pressure\": 170.4,\"Location\": \"Home\",\"Date\": \"date\"}";
        try {
            JSONDecoder jsonObject = new JSONDecoder(jsonString);
            sqLiteHelper.insertRecord(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        long row = sqLiteHelper.countRows("health");
        Toast.makeText(this, String.valueOf(row),Toast.LENGTH_SHORT).show();
        */
        long row = sqLiteHelper.countRows("health_count");
        Toast.makeText(this, String.valueOf(row),Toast.LENGTH_SHORT).show();
    }

    public void onClickReset(View view){
        alertDialog();
    }

    public void alertDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(PastDetailsActivity.this);
        builder.setMessage("Do you want to Reset the Database?").setTitle("Reset").setCancelable(false).setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
// TODO Auto-generated method stub
                //code to reset the db
                try {
                    sqLiteHelper.deleteAll("health");
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).setNegativeButton("no", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
// TODO Auto-generated method stub
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void onClickDB(View view){
/*
        ServerData serverData=new ServerData();
        String msg = serverData.jsonFormatString("f1","10",AppConstants.URL1);
        String num = serverData.rowNos(msg,"re");
        Toast.makeText(this,num,Toast.LENGTH_SHORT).show();
        Log.i("Received ",msg);
*/
        String m = String.valueOf(sqLiteHelper.valueOfHealthCount());
        Toast.makeText(this,m,Toast.LENGTH_SHORT).show();

    }
}
