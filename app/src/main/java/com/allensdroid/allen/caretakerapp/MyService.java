package com.allensdroid.allen.caretakerapp;

import android.app.Activity;
import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.UiThread;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONException;

import java.util.HashMap;


public class MyService extends Service{
    private static MqttClient client;
    String patient_username;
    NotificationCompat.Builder notification;
    private static final int uniqueID=456;
    String msg;
    SQLiteHelper sqLiteHelper;
    Context context;
    ServerData serverData;///server


    public MyService(Context context){
        this.context=context;
    }

    public MyService() {
        connect(AppConstants.SERVER);
        Log.i("Connected To MQTT ON : ",AppConstants.SERVER);

        serverData=new ServerData(); /////server


        notification = new NotificationCompat.Builder(this);
        notification.setAutoCancel(true);

        Log.i("patient_username: ","MySERVICE CONSTRUCTOR..");
    }

    @Override
    public void onCreate() {
        Log.i("STATUS :","ONCREATE");
        context=getApplicationContext();
        sqLiteHelper=new SQLiteHelper(context);
        patient_username=sqLiteHelper.userLogname().trim();
        Log.i("PATIENT USER_NAME :",patient_username);
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("On StartCommand : ","CYCLE");

        Log.i("Subscribed to: ",patient_username+AppConstants.TOPIC);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                //type your code here to run as service
                sub(patient_username+AppConstants.TOPIC);
                startChecking(patient_username);                    ///server checkings
            }
        };
    Thread thread=new Thread(runnable);
    thread.start();
    return Service.START_STICKY;
}


    @Override
    public void onDestroy() {
        Log.i("ON DESTROY : ","onDESTROY");
        try {
            client.disconnect();
        } catch (MqttException e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        Log.i("ONBINDER : ","ONBINDER");
        throw new UnsupportedOperationException("Not yet implemented");
    }


    public boolean sub(String topic) {
        try {
            client.subscribe(topic);
            client.setCallback(new MqttCallback() {
                //The message receives here
                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    Log.i("Received", topic + ": " + message);

                    msg = message.toString();
                    doInMessageArrived(msg);

                    Log.i("MESSAGE RECEIVED string", msg);

                }


                public void deliveryComplete(IMqttDeliveryToken token) {//Called when a outgoing publish is complete
                }

                @Override
                public void connectionLost(Throwable arg0) {
                    // TODO Auto-generated method stub

                }

            });

            return true;

        } catch (MqttPersistenceException e) {
            e.printStackTrace();

        } catch (MqttException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean connect(String url) {
        try {
            MemoryPersistence persistance = new MemoryPersistence();
            client = new MqttClient("tcp://" + url + ":1883", "client", persistance);
            client.connect();
            //client.subscribe("chat/+");
            //sub("patient/health");
            return true;
        } catch (MqttException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void setNotification(String status) {
        //build the notification
        notification.setSmallIcon(R.mipmap.ic_launcher);
        notification.setTicker("Health Status Alert");
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle("Health Status");
        notification.setContentText("Your patient health status is "+status);
        notification.setVibrate(new long[] { 0, 100, 200, 300 });
        notification.setSound(Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/" + R.raw.alert));

        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);

        //issueing the notifiacation
        NotificationManager notificationManager=(NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(uniqueID,notification.build());
    }

    public void toast(String tmsg){
        Toast.makeText(this,tmsg,Toast.LENGTH_SHORT).show();
    }


    public void doInMessageArrived(String msg){
        try {
            JSONDecoder jsonDecoder = new JSONDecoder(msg);
            setNotification(jsonDecoder.getCondition());//notification sets for user
            //toast(msg);
            //sqLiteHelper.insertRecord(jsonDecoder);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public String serverListenAndCount(String value){
        String msg = serverData.jsonFormatString("patient_userid",value,AppConstants.URL_HEALTHCOUNT);
        String num = serverData.rowNos(msg,"count");
        return num;
    }

    private void startChecking(final String patient_username_in){
            new Thread(new Runnable() {
                public void run() {
                    try {
                        int stored_row=sqLiteHelper.valueOfHealthCount(); //get the value from db
                        Log.i("STORED ROW ",String.valueOf(stored_row));
                        String x=serverListenAndCount(patient_username_in);
                        Log.i("STORED IN DB ",x);
                        if(!String.valueOf(stored_row).equals(x)) {
                            setNotification("new");
                            sqLiteHelper.insertCount(Integer.parseInt(x));
                            //store in db
                        }
                            Thread.sleep(5000);
                            startChecking(patient_username_in);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
    }


}
