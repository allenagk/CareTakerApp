package com.allensdroid.allen.caretakerapp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class JSONDecoder {

    String Condition, Occasion,Location,date;
    Double Heartrate, Pressure;

    public JSONDecoder(String jsonString) throws JSONException {
        try {
            JSONObject jsonHealth = new JSONObject(jsonString);
            setCondition(jsonHealth.getString("Condition"));
            setOccasion(jsonHealth.getString("Occasion"));
            setHeartrate(jsonHealth.getDouble("Heartrate"));
            setPressure(jsonHealth.getDouble("Pressure"));
            setLocation(jsonHealth.getString("Location"));
            setDate(jsonHealth.getString("Date"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getCondition() {
        return Condition;
    }

    public String getOccasion() {
        return Occasion;
    }

    public Double getHeartrate() {
        return Heartrate;
    }

    public Double getPressure() {
        return Pressure;
    }

    public String getLocation() {
        return Location;
    }

    public String getDate() {
        return date;
    }

    public void setCondition(String condition) {
        Condition = condition;
    }

    public void setOccasion(String occasion) {
        Occasion = occasion;
    }

    public void setHeartrate(Double heartrate) {
        Heartrate = heartrate;
    }

    public void setPressure(Double pressure) {
        Pressure = pressure;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
