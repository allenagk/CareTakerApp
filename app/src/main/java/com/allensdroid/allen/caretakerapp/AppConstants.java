package com.allensdroid.allen.caretakerapp;

/**
 * Created by allen on 6/19/16.
 */
public class AppConstants {
    public static String SERVER = "54.68.239.76"; //public ip of the ec2
    public static final String TOPIC = "/health";

    public static final String URL_HEALTHCOUNT="http://54.68.239.76/New/webapp/health_count.php";
    public static final String URL_ROWS="http://54.68.239.76/New/webapp/row_nos.php";
    public static final String URL_USERCHECK="http://54.68.239.76/New/webapp/check_userid.php";
}
