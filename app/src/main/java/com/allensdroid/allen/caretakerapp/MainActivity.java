package com.allensdroid.allen.caretakerapp;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.CursorIndexOutOfBoundsException;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    public static String patient_username;
    TextView textLogstatus; // textView2,textView4,textView6,textView8;
    NotificationCompat.Builder notification;
    SessionManager session;
    SQLiteHelper sqLiteHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sqLiteHelper = new SQLiteHelper(this);

        session=new SessionManager(getApplicationContext());
        textLogstatus=(TextView)findViewById(R.id.textLogstatus);

        notification = new NotificationCompat.Builder(this);
        notification.setAutoCancel(true);

        /**
         * Call this function whenever you want to check user login
         * This will redirect user to LoginActivity is he is not
         * logged in
         * */
        Log.i("STATUS ","CHECK LOGIN");

        session.checkLogin(); //starting login activity if user not logged in otherwise code flow..

        // get user data from session
        HashMap<String, String> user = session.getUserDetails();  //get the user as patient_username
        Log.i("STATUS ","HASHMAP PASS");

        // name
        try{
            patient_username = user.get(SessionManager.KEY_NAME).trim();
        }
        catch (RuntimeException e){
            e.printStackTrace();
        }
        textLogstatus.setText("Logged in: "+patient_username);
        Log.i("STATUS ","SET TEXT");

        setTable();
        Log.i("STATUS ","SERVICE SRART flag");

        if(sqLiteHelper.countRows("user")>0) {
            Intent intent = new Intent(this, MyService.class);
            startService(intent);
            Log.i("STATUS ","SERVICE SRARTED");
        }

        Log.i("STATUS ","SERVICE END flag");

    }


    private void view_About() {
// TODO Auto-generated method stub
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater factory = LayoutInflater.from(MainActivity.this);
        final View textEntryview = factory.inflate(R.layout.about, null);
        alt_bld.setView(textEntryview).setCancelable(true).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
// TODO Auto-generated method stub
                dialog.cancel();
            }
        });
        AlertDialog alert = alt_bld.create();
        alert.setTitle("About");
        alert.setIcon(R.drawable.ic_launcher);
        alert.show();
    }

    public void pastDetailOnclick(View view){
        Intent i = new Intent(getApplicationContext(), PastDetailsActivity.class);
        startActivity(i);
        Log.d("MAINpatientusrnme",patient_username);
    }

    public void logoutOnclick(View view){
        logout();
            //sub(patient_username+AppConstants.TOPIC);
    }

    public void logout(){
        try {
            Log.i("STATUS : ", "LOGOUT CALLED");
            session.logoutUser();
            sqLiteHelper.deleteAll("user");
            sqLiteHelper.deleteAll("health");
            sqLiteHelper.deleteAll("health_count");
            Intent intent = new Intent(MainActivity.this, MyService.class);
            stopService(intent);
            finish();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch(item.getItemId()){
            case R.id.action_logout:
                logout();
                return true;
            case R.id.action_about:
                view_About();
                return true;
            case R.id.action_refresh:
                setTable();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setTable(){
        TextView textViewConndition=(TextView)findViewById(R.id.textView2);
        TextView textViewHeartrate=(TextView)findViewById(R.id.textView4);
        TextView textViewPressure=(TextView)findViewById(R.id.textView6);
        TextView textViewLocation=(TextView)findViewById(R.id.textView8);
        TextView textViewDate=(TextView)findViewById(R.id.textView10);
        TextView textViewOccasion=(TextView)findViewById(R.id.textView12);

        //by getFirstRow method update table to latest health details
        try {
            textViewConndition.setText(sqLiteHelper.getFirstRow(5));
            textViewHeartrate.setText(sqLiteHelper.getFirstRow(2));
            textViewPressure.setText(sqLiteHelper.getFirstRow(3));
            textViewLocation.setText(sqLiteHelper.getFirstRow(4));
            textViewDate.setText(sqLiteHelper.getFirstRow(1));
            textViewOccasion.setText(sqLiteHelper.getFirstRow(6));
        }catch (CursorIndexOutOfBoundsException e){
            e.printStackTrace();
        }

    }





    /*

        //Subscribing to all the topics & Receiving messages from them


    public static boolean connect(String url) {
        try {
            MemoryPersistence persistance = new MemoryPersistence();
            client = new MqttClient("tcp://" + url + ":1883", "client", persistance);
            client.connect();
            //client.subscribe("chat/+");
            //sub("patient/health");
            return true;
        } catch (MqttException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    public boolean sub(String topic) {
        try {
            client.subscribe(topic);
            client.setCallback(new MqttCallback() {

                //The message receives here
                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    Log.d("Received", topic + ": " + message);

                    MainActivity.msg = message.toString();
                    MainActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            try {
                                JSONDecoder jsonDecoder=new JSONDecoder(msg);
                                sqLiteHelper.insertRecord(jsonDecoder);
                                setNotification(jsonDecoder.getCondition());//notification sets for user
                                setTable();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();//toast message
                            // textView.setText(msg);//settextview message
                        }
                    });
                    Log.e("MESSAGE RECEIVED", message.toString());

                }


                public void deliveryComplete(IMqttDeliveryToken token) {//Called when a outgoing publish is complete
                }

                @Override
                public void connectionLost(Throwable arg0) {
                    // TODO Auto-generated method stub

                }

            });

            return true;

        } catch (MqttPersistenceException e) {
            e.printStackTrace();

        } catch (MqttException e) {
            e.printStackTrace();
        }
        return false;
    }

*/

}

