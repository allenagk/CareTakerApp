package com.allensdroid.allen.caretakerapp;

import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by allen on 6/26/16.
 */
public class ServerData {
    String result = null;
    InputStream is = null;

    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

    public ServerData(){
        StrictMode.setThreadPolicy(policy);
    }


    public String jsonFormatString(String column_name, String column_value,String url){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair(column_name,column_value));
        try
        {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(url);			//ec2 insert.php locatiobn
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));	//pass 'f1' with value v1
            HttpResponse response = httpclient.execute(httppost);      //execute with passing valuees
            HttpEntity entity = response.getEntity();
            is = entity.getContent();				   //get the content from php file nad pass to bufferdreader

            Log.e("log_tag", "connection success ");
            //   Toast.makeText(getApplicationContext(), "pass", Toast.LENGTH_SHORT).show();
        }
        catch(Exception e){
            Log.e("log_tag", "Error in http connection : "+e.toString());
        }
        //convert response to string
        try{
            BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8); //content from php
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
                //  Toast.makeText(getApplicationContext(), "Input Reading pass", Toast.LENGTH_SHORT).show();
            }
            is.close();

            result=sb.toString();
        }
        catch(Exception e)
        {
            Log.e("log_tag", "Error converting result "+e.toString());
        }
        return result;
    }


    public  String encodeTableToJSON(String result) {
        String msg=null;
        int x=0;
        //parse json data
        try {
            JSONObject object = new JSONObject(result);
            String ch = object.getString("re");
            x=object.getInt("nm");
            Log.i("Valux", String.valueOf(x));
            if (ch.equals("success")) {
                //SET LOOP TO GET OBJECT FROM 0 TO 10 TO DISPLAY LAST 10 DETAILS
                JSONObject no = object.getJSONObject("0");
                //long q=object.getLong("f1");
                String w = no.getString("f2");
                long e = no.getLong("f3");

                //editText1.setText(w);
                //String myString = NumberFormat.getInstance().format(e);

                msg=w+" "+e;

                //editText2.setText(myString);

            } else if(ch.equals("No_record")){
                Log.i("STATUS ","NO_RECORD");
            }else if(ch.equals("Null_value")) {
                Log.i("STATUS ","NULL_VALUE");
            }
        } catch (JSONException e) {
            Log.e("log_tag", "Error parsing data " + e.toString());
        }
        return String.valueOf(x);
    }

    public String rowNos(String jsonmsg,String findObjName){
        String rply = null;
        try {
            JSONObject object = new JSONObject(jsonmsg);
            rply= object.getString(findObjName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return rply;
    }
}
