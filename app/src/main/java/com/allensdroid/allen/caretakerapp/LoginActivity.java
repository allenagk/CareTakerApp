package com.allensdroid.allen.caretakerapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    EditText editUserID;
    Button bSubscribe;
    SessionManager session;
    SQLiteHelper sqLiteHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sqLiteHelper=new SQLiteHelper(this);
        session = new SessionManager(getApplicationContext());
        editUserID=(EditText)findViewById(R.id.editUserID);
        bSubscribe=(Button)findViewById(R.id.bSubscribe);
    }

    public void subscribe(View view){
        String patient_userID=editUserID.getText().toString();
        if(patient_userID.trim().length()>0){
            ServerData serverData=new ServerData();
            String msg = serverData.jsonFormatString("patient_userid",patient_userID,AppConstants.URL_USERCHECK);
            String num = serverData.rowNos(msg,"re");
            Log.i("Message ",num);
            if(num.equals("success")) {
                Log.i("STATUS ","SUCCESS");
                sqLiteHelper.insertPatient_UserId(patient_userID);
                //set health count_table for this patient from the server
                String c_msg=serverData.jsonFormatString("patient_userid",patient_userID,AppConstants.URL_HEALTHCOUNT);
                String count=serverData.rowNos(c_msg,"count");
                Log.i("COUNTS HEALTHD ",count);
                sqLiteHelper.insertCount(Integer.parseInt(count));
                //update local health table for latest 10 details from the server
                ///getValues from json array
                session.createLoginSession(patient_userID);
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                finish();
            }else{
                Toast.makeText(this,"Invalid patient userID, Contact support center for help",Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(this,"Enter patient id",Toast.LENGTH_SHORT).show();
        }

    }



}
